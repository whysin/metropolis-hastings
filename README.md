#### Project: Metropolis Hastings

#### Name: Noah Lindley & Pedro H. F. dos Santos

#### Instructions:
    - execute serially : make serial
    - execute openmp : make openmp
    - execute MPI : make mpi

    - exp1 can be ran by uncommenting lines 102-105 (make openmp-exp1 or make mpi-exp1)
    - exp2 can be ran by uncommenting lines 109
    - Note: all other couts should be commented