
serial:
	icpc -Itrng/ metropolis_serial.cc -o serial
	./serial

openmp:
	icpc -Itrng/ -qopenmp -O2 -xhost metropolis_OpenMP.cc -o openmp
	numactl --cpunodebind=0 --preferred=0 ./openmp

mpi:
	mpicxx -Itrng/ metropolis_MPI.cc -o mpi
	ibrun -np 4 mpi

clean:
	rm -f serial
	rm -f openmp
	rm -f mpi

openmp-exp1:
	icpc -Itrng/ -qopenmp -O2 -xhost metropolis_OpenMP.cc -o openmp
	numactl --cpunodebind=0 --preferred=0 ./openmp > exp1.csv

openmp-exp2:
	icpc -Itrng/ -qopenmp -O2 -xhost metropolis_OpenMP.cc -o openmp
	
	for number in 1 2 4 8 16 32 64 128 256 512; do \
    	export OMP_NUM_THREADS=$$number ; \
    	numactl --cpunodebind=0 --preferred=0 ./openmp ; \
	done

mpi-exp1:
	mpicxx -Itrng/ metropolis_MPI.cc -o mpi
	ibrun -np 8 ./mpi > mpi_exp1.csv

mpi-exp2:
	mpicxx -Itrng/ metropolis_MPI.cc -o mpi

	for number in 1 2 4 8 16 32 64 128 256 512; do \
		ibrun -np $$number mpi ; \
	done