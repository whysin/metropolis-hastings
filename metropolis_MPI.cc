#include <normal_dist.hpp>
#include <uniform01_dist.hpp>
#include <yarn2.cc>

#include <iostream>
#include <string>
#include <random>
#include <cstdint>
#include <vector>
#include <fstream>
#include <algorithm>
#include <cstdlib>
#include <cmath>
#include <ctime>

#include <mpi.h>

void print_normal(trng::normal_dist<double> normal);

double log_prior(double alpha, double beta);
double log_likelihood(double alpha, double beta);
double log_density(double alpha, double beta);
double factorial(int val);
double posterior_mean(std::vector<double> v, uint64_t offset);

int metropolis_hastings(uint64_t n, uint64_t burnin);

trng::uniform01_dist<double> uniform_real_dist;
trng::normal_dist<double> normal_dist(0, 1.0);
trng::normal_dist<double> norm_inits(0, 10.0);
const double SIGMA = 100.0;

int main(int argc, char *argv[]){
    int num_threads = metropolis_hastings(100000, 0);
}

int metropolis_hastings(uint64_t sample_count, uint64_t burnin){
    
    int rank, size;
    double t0, t1;
    MPI_Init(0, 0);
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    
    MPI_Barrier(MPI_COMM_WORLD);
    t0 = MPI_Wtime();
    
    std::vector<double> alpha(sample_count), beta(sample_count);
    uint64_t accepted_count;
    double old_alpha, old_beta, new_alpha, new_beta, new_density, old_density, difference;

    //Creating independent process random number streams
    trng::yarn2 stream[size];
	stream[rank].split(size,rank);
    
    alpha = std::vector<double> (sample_count);
    beta = std::vector<double> (sample_count);
    //Randomize initial starts of theta one and two
    alpha[0] = norm_inits(stream[rank]);
    beta[0] = norm_inits(stream[rank]);

    accepted_count = 0;
    //doing a random walk
    for(uint64_t sample_idx = 1; sample_idx <= sample_count; ++sample_idx){

        //Get the old alpha and old beta
        old_alpha = alpha[sample_idx - 1];
        old_beta = beta[sample_idx - 1];
        
        //Propose a new position
        new_alpha = old_alpha + normal_dist(stream[rank]);
        new_beta = old_beta + normal_dist(stream[rank]);

        //Calculate the log-density for the new and old alpha & beta
        new_density = log_density(new_alpha, new_beta);
        old_density = log_density(old_alpha, old_beta);

        //Calculating the log-acceptance probability
        difference = new_density - old_density;

        /*Metropolis Accept/Reject*/

        // Accept and set the proposed as the current position
        if(log(uniform_real_dist(stream[rank])) < difference){
            alpha[sample_idx] = new_alpha;
            beta[sample_idx] = new_beta;
            ++accepted_count;
        // Reject and set the old as the current position
        }else{
            alpha[sample_idx] = old_alpha;
            beta[sample_idx] = old_beta;
        }
    }
    MPI_Barrier(MPI_COMM_WORLD); // wait for all threads to be done

    
    t1 = MPI_Wtime();
    if(rank == 0){
        std::cout << size << ";" << t1-t0 << std::endl;
    }


    //Critical section
    for(int i = 0; i < size; ++i){
        if(i == rank){
            // std::cout << "initial alpha & beta: " << alpha[0] << " & " << beta[0] << std::endl;
            // std::cout << "posterior mean of alpha: " << posterior_mean(alpha, burnin) << std::endl;
            // std::cout << "posterior mean of beta: " << posterior_mean(beta, burnin) << std::endl;
            // std::cout << "Acceptance Ratio: " << accepted_count / (double) sample_count << std::endl;

            // std::cout << "thread_num;alpha;beta" << std::endl;
            // for(int i = 0; i < sample_count; ++i){
            //     std::cout << rank << ";" << alpha[i] << ";" << beta[i] << std::endl;
            // }
        }
        MPI_Barrier(MPI_COMM_WORLD);
    }
    
    MPI_Finalize();
    return size;
}

double posterior_mean(std::vector<double> v, uint64_t offset){
    uint64_t count = 0;
    double sum = 0.0;
    for(int i = offset; i < v.size(); ++i){
        sum += v[i];
        ++count;
    }
    return sum/count;
}


double log_density(double alpha, double beta){
    return log_likelihood(alpha, beta) + log_prior(alpha, beta);
}

double log_likelihood(double alpha, double beta){
    std::vector<double> x_i = {-.86, -.30, -0.05, 0.73};
    std::vector<int> n_i = {6, 5, 5, 5};
    std::vector<int> y_i = {1, 2, 3, 5};
    std::vector<double> pi_i(4);
    for(int i = 0; i < 4; ++i){
        pi_i[i] = 1/(1+exp(alpha + (beta * x_i[i])));
        // std::cout << pi_i[i] << std::endl;
    }
    std::vector<double> factorial_i(4);
    double result = 0;
    for(int i = 0; i < 4; ++i){
        factorial_i[i] = factorial(n_i[i]) - factorial(y_i[i]) - factorial(n_i[i]-y_i[i]);
        result += factorial_i[i] + (y_i[i] * log(pi_i[i])) + ((n_i[i]-y_i[i]) * log(1-pi_i[i]));
    }
    return result;
}

double factorial(int val){
    if(val < 1){
        return 0;
    }else{
        return log(val) + factorial(val-1);
    }
}

double log_prior(double alpha, double beta){
	return (-1 * log(2*M_PI*SIGMA)) + (-1/(2*SIGMA))*((alpha*alpha)+(beta*beta));
}




void print_normal(trng::normal_dist<double> normal){
    int p[10]={};
    const int nrolls=10000;  // number of experiments
    const int nstars=100;    // maximum number of stars to distribute

    std::default_random_engine generator;

    for (int i=0; i<nrolls; ++i) {
        double number = normal(generator);
        if ((number>=0.0)&&(number<10.0)) ++p[int(number)];
    }


    std::cout << "normal_distribution (0.0,0.1):" << std::endl;

    for (int i=0; i<10; ++i) {
        std::cout << i << "-" << (i+1) << ": ";
        std::cout << std::string(p[i]*nstars/nrolls,'*') << std::endl;
    }
}