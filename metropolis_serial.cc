#include <normal_dist.hpp>
#include <uniform01_dist.hpp>
#include <yarn2.cc>

#include <iostream>
#include <string>
#include <random>
#include <cstdint>
#include <vector>
#include <fstream>
#include <algorithm>
#include <cstdlib>
#include <cmath>
#include <ctime>

void print_normal(trng::normal_dist<double> normal);

double log_prior(double alpha, double beta);
double log_likelihood(double alpha, double beta);
double log_density(double alpha, double beta);
double factorial(int val);
double posterior_mean(std::vector<double> v, uint64_t offset);

void metropolis_hastings(uint64_t n, uint64_t burnin);

trng::uniform01_dist<double> uniform_real_dist;
trng::normal_dist<double> normal_dist(0, 1.0);
const double SIGMA = 100.0;

int main(int argc, char *argv[]){
    metropolis_hastings(100000, 0);
}

void metropolis_hastings(uint64_t sample_count, uint64_t burnin){
    std::vector<double> alpha(sample_count), beta(sample_count);
    int size = 1;

    //Creating random numbers
    trng::yarn2 stream[size];
	for (int i = 0; i < size; i++)
	{
		stream[i].split(size,i);
	}

    //Randomize initial starts of theta one and two
    alpha[0] = uniform_real_dist(stream[0]);
    beta[0] = uniform_real_dist(stream[0]);

    // std::cout << alpha[0] << std::endl;
    // std::cout << beta[0] << std::endl;

    uint64_t accepted_count = 0;
    //doing a random walk
    for(uint64_t sample_idx = 1; sample_idx <= sample_count; ++sample_idx){
        double old_alpha = alpha[sample_idx - 1];
        double old_beta = beta[sample_idx - 1];

        double new_alpha = old_alpha + normal_dist(stream[0]);
        double new_beta = old_beta + normal_dist(stream[0]);

        double new_density = log_density(new_alpha, new_beta);
        double old_density = log_density(old_alpha, old_beta);

        double difference = new_density - old_density;

        // Metropolis Accept/Reject
        if(log(uniform_real_dist(stream[0])) < difference){
            alpha[sample_idx] = new_alpha;
            beta[sample_idx] = new_beta;
            ++accepted_count;
        }else{
            alpha[sample_idx] = old_alpha;
            beta[sample_idx] = old_beta;
        }
    }

    std::cout << "posterior mean of alpha: " << posterior_mean(alpha, burnin) << std::endl;
    std::cout << "posterior mean of beta: " << posterior_mean(beta, burnin) << std::endl;
    std::cout << "Acceptance Ratio: " << accepted_count / (double) sample_count << std::endl;

}

double posterior_mean(std::vector<double> v, uint64_t offset){
    uint64_t count = 0;
    double sum = 0.0;
    for(int i = offset; i < v.size(); ++i){
        sum += v[i];
        ++count;
    }
    return sum/count;
}


double log_density(double alpha, double beta){
    return log_likelihood(alpha, beta) + log_prior(alpha, beta);
}

double log_likelihood(double alpha, double beta){
    std::vector<double> x_i = {-.86, -.30, -0.05, 0.73};
    std::vector<int> n_i = {6, 5, 5, 5};
    std::vector<int> y_i = {1, 2, 3, 5};
    std::vector<double> pi_i(4);
    for(int i = 0; i < 4; ++i){
        pi_i[i] = 1/(1+exp(alpha + (beta * x_i[i])));
        // std::cout << pi_i[i] << std::endl;
    }
    std::vector<double> factorial_i(4);
    double result = 0;
    for(int i = 0; i < 4; ++i){
        factorial_i[i] = factorial(n_i[i]) - factorial(y_i[i]) - factorial(n_i[i]-y_i[i]);
        result += factorial_i[i] + (y_i[i] * log(pi_i[i])) + ((n_i[i]-y_i[i]) * log(1-pi_i[i]));
    }
    return result;
}

double factorial(int val){
    if(val < 1){
        return 0;
    }else{
        return log(val) + factorial(val-1);
    }
}

double log_prior(double alpha, double beta){
	return (-1 * log(2*M_PI*SIGMA)) + (-1/(2*SIGMA))*((alpha*alpha)+(beta*beta));
}




void print_normal(trng::normal_dist<double> normal){
    int p[10]={};
    const int nrolls=10000;  // number of experiments
    const int nstars=100;    // maximum number of stars to distribute

    std::default_random_engine generator;

    for (int i=0; i<nrolls; ++i) {
        double number = normal(generator);
        if ((number>=0.0)&&(number<10.0)) ++p[int(number)];
    }


    std::cout << "normal_distribution (0.0,0.1):" << std::endl;

    for (int i=0; i<10; ++i) {
        std::cout << i << "-" << (i+1) << ": ";
        std::cout << std::string(p[i]*nstars/nrolls,'*') << std::endl;
    }
}